# Utazaselemzes

Ezen project CSV fájlok feldolgozására lesz alkalmas amely buszjáratok és utasairól tartalmaz információkat. Ezen adatokról a rendszerezett eltárolás után statisztikákat lehet majd készíteni. A tárolt adatokat és formátumát lásd a mintabemenet.csv fájlban.

A maven-nel legyártott fat jar a következő képpen futtatható a terminálból:
`java -jar Utazaselemzes-jar-with-dependencies.jar mintabemenet.csv out.json`