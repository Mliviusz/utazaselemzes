package hu.liviusz.utazaselemzes;

import java.io.IOException;

public interface Statisztikak {

    /**
     *
     * A statisztika itt nyeri ki az UtazásiAdat-ban rendszerezett adatból a neki szükségeseket és összefüggéseket.
     *
     * @param utazasAdat az a rendszerezettt adatokat tároló UtazásiAdatok objektum aliasa
     */
    void feldolgoz(UtazasAdatok utazasAdat);

    /**
     *
     * A feldogoz methodusban kinyert és összeállított adatokat kiírja a kívánt fájlnéven.
     *
     * @param outFileNeve az elkészített statisztikát tartalmazó fájlnak megadott neve
     *
     * @throws IOException
     */
    void outputKiirasa(String outFileNeve) throws IOException;
}
