package hu.liviusz.utazaselemzes;

import java.util.LinkedList;
import java.util.List;

/**
 * A járat rendszerbeli azonosítóját és tömegközlekedési járatszámát tárolja, az ő hozzá fűződő felszállások összességével
 * Primary Key a jaratId
 */
public class Jarat {

    private String jaratId;
    private String jaratSzam;
    private List<Felszallas> felszallasok;

    public Jarat(String jaratId, String jaratSzam) {
        this.jaratId = jaratId;
        this.jaratSzam = jaratSzam;
        felszallasok = new LinkedList<>();
    }

    public String getJaratId() { return jaratId; }

    public void setJaratId(String jaratId) { this.jaratId = jaratId; }

    public List<Felszallas> getFelszallasok() { return felszallasok; }

    public void addFelszallas(Felszallas felszallas) {
        this.felszallasok.add(felszallas);
    }

}
