package hu.liviusz.utazaselemzes;

import java.util.LinkedList;
import java.util.List;

/**
 * A megálló rendszerbeli azonosítóját és tömegközlekedési címét (pl.: Deák Ferenc tér) tárolja, az ő hozzá fűződő felszállások összességével
 * Primary Key a jaratId
 */
public class Megallo {

    private String megalloId;
    private String megalloCim;
    private List<Felszallas> felszallasok;

    public Megallo(String megalloId, String megalloCim){
        this.megalloId = megalloId;
        this.megalloCim = megalloCim;
        felszallasok = new LinkedList<>();
    }

    public String getMegalloId() { return megalloId; }

    public void setMegalloId(String megalloId) { this.megalloId = megalloId; }

    public List<Felszallas> getFelszallasok() { return felszallasok; }

    public void addFelszallas(Felszallas felszallas) {
        felszallasok.add(felszallas);
    }

}
