package hu.liviusz.utazaselemzes;

import java.util.*;

/**
 * Az UtazasAdatok class felel a bemeneti csv adatainak rendszerezéséért és tárolásáért.
 * Az adatokat reprezentáló objektumokat List és Map segítségével tárolja el.
 */
public class UtazasAdatok {

    private List<Felszallas> felszallasok;
    private Map<String, Busz> buszaim;
    private Map<String, Jarat> jarataim;
    private Map<String, Megallo> megalloim;
    private Map<String, Utas> utasaim;

    public UtazasAdatok() {
        felszallasok = new LinkedList<>();
        buszaim = new HashMap<>();
        jarataim = new HashMap<>();
        megalloim = new HashMap<>();
        utasaim = new HashMap<>();
    }

    public Map<String, Busz> getBuszaim() {
        return buszaim;
    }

    public Map<String, Jarat> getJarataim() {
        return jarataim;
    }

    public Map<String, Megallo> getMegalloim() {
        return megalloim;
    }

    public Map<String, Utas> getUtasaim() {
        return utasaim;
    }

    /**
     *
     * Ezem methodus felel a fájl sorainak egyesével való elemzéséért, azon új "felszállást" létrehozni és hozzáadni a gyűjtő listához.
     *
     * @param utazasAdatScan a bemeneti fájl-t (mintabemenet.csv) kezelő Scanner aliasa.
     *
     * @throws NemFeldolgozhatoAdatException
     */
    public void feldolgoz(Scanner utazasAdatScan) throws NemFeldolgozhatoAdatException {

        // Sorok beolvasása egyesével, de az 1. sor header-t kihagyjuk
        utazasAdatScan.nextLine();
        int sorszamSzamlalo  = 1;
        while (utazasAdatScan.hasNextLine()) {
            sorszamSzamlalo++;
            String sor = utazasAdatScan.nextLine();
            String[] adatok = sor.split(";");
            if (adatok.length < 12) {
                String hibaUzenet = String.format("HIBA: Nem dolgozhato fel a fajl, mert a [%s.] sora hibas!", sorszamSzamlalo);
                throw new NemFeldolgozhatoAdatException(hibaUzenet);
            }
            String rendszam = adatok[0];
            String alvazSzam = adatok[1];
            String jaratSzam = adatok[2];
            String jaratId = adatok[3];
            String megalloId = adatok[4];
            String megalloCim = adatok[5];
            String honap = adatok[6];
            String nap = adatok[7];
            String ora = adatok[8];
            String perc = adatok[9];
            String berletId = adatok[10];
            String berletFNev = adatok[11];

            Felszallas ujFelszallas = new Felszallas(honap, nap, ora, perc);
            felszallasok.add(ujFelszallas);

            feldolgozBusz(rendszam, alvazSzam, ujFelszallas);
            feldolgozJarat(jaratSzam, jaratId, ujFelszallas);
            feldolgozMegallo(megalloId, megalloCim, ujFelszallas);
            feldolgozUtas(berletId, berletFNev, ujFelszallas);
        }

    }

    /**
     *
     * A busszal kapcsolatos információk feldolgozásáért felel.
     * Megvizsgálja található-e már busz az adott adatokkal, ha nem létrehozza, aztán összekapcsolja (2 irányúan) az ujFelszállással
     *
     * @param rendszam a busz rendszáma
     * @param alvazSzam a busz alvázszáma
     * @param ujFelszallas azon "felszállás" ami a bemenet adott sorának adatait összesíti/összekapcsolja
     */
    private void feldolgozBusz(String rendszam, String alvazSzam, Felszallas ujFelszallas) {
        Busz busz;
        if (!buszaim.containsKey(rendszam)) {
            busz = new Busz(rendszam, alvazSzam);
            buszaim.put(rendszam, busz);
        } else {
            busz = buszaim.get(rendszam);
        }
        ujFelszallas.setBuszom(busz);
        busz.addFelszallas(ujFelszallas);
    }

    /**
     *
     * A járattal kapcsolatos információk feldolgozásáért felel.
     * Megvizsgálja található-e már járat az adott adatokkal, ha nem létrehozza, aztán összekapcsolja (2 irányúan) az ujFelszállással
     *
     * @param jaratSzam a járat tömegközlekedési vonalszáma (pl.: 4-es villamos)
     * @param jaratId a járatnak a rendszerbeli azonosítója
     * @param ujFelszallas azon "felszállás" ami a bemenet adott sorának adatait összesíti/összekapcsolja
     */
    private void feldolgozJarat(String jaratSzam, String jaratId, Felszallas ujFelszallas) {
        Jarat jarat;
        if (!jarataim.containsKey(jaratId)) {
            jarat = new Jarat(jaratId, jaratSzam);
            jarataim.put(jaratId, jarat);
        } else {
            jarat = jarataim.get(jaratId);
        }
        ujFelszallas.setJaratom(jarat);
        jarat.addFelszallas(ujFelszallas);
    }

    /**
     *
     * A megallóval kapcsolatos információk feldolgozásáért felel.
     * Megvizsgálja található-e már megálló az adott adatokkal, ha nem létrehozza, aztán összekapcsolja (2 irányúan) az ujFelszállással
     *
     * @param megalloId a megállónak a rendszerbeli azonosítója
     * @param megalloCim a megállónak a tömegközlekedésben használt neve (pl. Deák Ferenc tér)
     * @param ujFelszallas azon "felszállás" ami a bemenet adott sorának adatait összesíti/összekapcsolja
     */
    private void feldolgozMegallo(String megalloId, String megalloCim, Felszallas ujFelszallas) {
        Megallo megallo;
        if (!megalloim.containsKey(megalloId)) {
            megallo = new Megallo(megalloId, megalloCim);
            megalloim.put(megalloId, megallo);
        } else {
            megallo = megalloim.get(megalloId);
        }
        ujFelszallas.setMegallom(megallo);
        megallo.addFelszallas(ujFelszallas);
    }

    /**
     *
     * Az utassal kapcsolatos információk feldolgozásáért felel.
     * Megvizsgálja található-e már utas az adott adatokkal, ha nem létrehozza, aztán összekapcsolja (2 irányúan) az ujFelszállással
     *
     * @param berletId az utashoz tartozó bérletnek a rendszerbeli azonosítója
     * @param berletFNev az adott bérlettel rendelkező utas felhasználó neve
     * @param ujFelszallas azon "felszállás" ami a bemenet adott sorának adatait összesíti/összekapcsolja
     */
    private void feldolgozUtas(String berletId, String berletFNev, Felszallas ujFelszallas) {
        Utas utas;
        if (!utasaim.containsKey(berletId)) {
            utas = new Utas(berletId, berletFNev);
            utasaim.put(berletId, utas);
        } else {
            utas = utasaim.get(berletId);
        }
        ujFelszallas.setUtasom(utas);
        utas.addFelszallas(ujFelszallas);
    }

}
