package hu.liviusz.utazaselemzes;

/**
 * Azon hibaesetre jelentkező error üzenet amikor a bemenet egyik sora túl kevés adatot tartalmaz.
 */
public class NemFeldolgozhatoAdatException extends Exception {

    public NemFeldolgozhatoAdatException(String hibaUzenet) {
        super(hibaUzenet);
    }
}
