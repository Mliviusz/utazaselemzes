package hu.liviusz.utazaselemzes;

/**
 *  Egy "felszállás", azaz 1 sor adatait tároló/összakapcsoló class
 *  tárolja a felszállás időpontját és az összes hozzá tartozó körülmény (Ki melyik járatra melyik buszon szált fel)
 *  Primary Key-jel nem rendelkezik.
 */
public class Felszallas {

    private Busz buszom;
    private Jarat jaratom;
    private Megallo megallom;
    private Utas utasom;

    private String honap;
    private String nap;
    private String ora;
    private String perc;

    public Felszallas(String honap,String nap, String ora, String perc) {
        this.honap = honap;
        this.nap = nap;
        this.ora = ora;
        this.perc = perc;
    }

    public Busz getBuszom() { return buszom; }

    public void setBuszom(Busz buszom) { this.buszom = buszom; }

    public Jarat getJaratom() { return jaratom; }

    public void setJaratom(Jarat jaratom) { this.jaratom = jaratom; }

    public Megallo getMegallom() { return megallom; }

    public void setMegallom(Megallo megallom) { this.megallom = megallom; }

    public Utas getUtasom() { return utasom; }

    public void setUtasom(Utas utasom) { this.utasom = utasom; }
}
