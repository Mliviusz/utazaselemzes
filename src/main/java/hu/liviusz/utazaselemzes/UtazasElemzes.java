package hu.liviusz.utazaselemzes;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class UtazasElemzes {
    public static void main(String[] args) {

        if (args.length < 2) {
            System.out.println("Usage: UtazasElemezes INPUT_CSV OUTPUT_FILE");
            System.exit(-1);
        }

        try {
            File utazasAdatFajl = new File(args[0]);
            Scanner utazasAdatScan = new Scanner(utazasAdatFajl);

            UtazasAdatok utazasiAdat = new UtazasAdatok();
            utazasiAdat.feldolgoz(utazasAdatScan);
            Statisztikak jsonStat = new JsonStatisztikak();
            jsonStat.feldolgoz(utazasiAdat);
            jsonStat.outputKiirasa(args[1]);

            utazasAdatScan.close();
        } catch (FileNotFoundException  e) {
            System.out.println("HIBA: Nincs meg az input fájl!. " + args[0]);
        } catch (NemFeldolgozhatoAdatException e) {
            e.printStackTrace();
            //System.out.println("HIBA: Nem dolgozhato fel a fajl, mert az adat hibas sort tartalmaz!");
        } catch (IOException e){
            System.out.println("IO HIBA: " + e);
        }
    }
}
