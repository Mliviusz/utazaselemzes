package hu.liviusz.utazaselemzes;

import java.util.LinkedList;
import java.util.List;

/**
 * A busz rendszámát és alvázszámát tárolja, az ő hozzá fűződő felszállások összességével
 * Primary Key a rendszam
 */
public class Busz {

    private String rendszam;
    private String alvazSzam;
    private List<Felszallas> felszallasok ;

    public Busz(String rendszam, String alvazSzam) {
        this.rendszam = rendszam;
        this.alvazSzam = alvazSzam;
        felszallasok = new LinkedList<>();
    }

    public String getRendszam() {
        return rendszam;
    }

    public void setRendszam(String rendszam) {
        this.rendszam = rendszam;
    }

    public List<Felszallas> getFelszallasok() {
        return felszallasok;
    }

    public void addFelszallas(Felszallas felszallas) {
        felszallasok.add(felszallas);
    }
}
