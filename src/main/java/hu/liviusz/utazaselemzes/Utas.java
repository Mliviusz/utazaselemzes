package hu.liviusz.utazaselemzes;

import java.util.LinkedList;
import java.util.List;

/**
 * Az utas nevét és a bérletének számát tartalmazza, az ő hozzá fűződő felszállások összességével
 * Primary Key a berletSzam
 */
public class Utas {

    private String berletSzam;
    private String berletFNev;
    private List<Felszallas> felszallasok;

    public Utas(String berletSzam, String berletFNev) {
        this.berletSzam = berletSzam;
        this.berletFNev = berletFNev;
        felszallasok = new LinkedList<>();
    }

    public String getBerletSzam() {
        return berletSzam;
    }

    public void setBerletSzam(String berletSzam) {
        this.berletSzam = berletSzam;
    }

    public List<Felszallas> getFelszallasok() { return felszallasok; }

    public void addFelszallas(Felszallas felszallas) {
        this.felszallasok.add(felszallas);
    }

}
