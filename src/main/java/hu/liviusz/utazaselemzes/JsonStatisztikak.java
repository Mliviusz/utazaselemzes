package hu.liviusz.utazaselemzes;

import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


/**
 * JSON fájlokba készített statisztikákat implementáló class
 */
public class JsonStatisztikak implements Statisztikak {

    private JSONObject bemenetek;

    @Override
    public  void feldolgoz(UtazasAdatok utazasAdat) {
        bemenetek = new JSONObject();

        bemenetek.put("Statisztika1: Buszfelszallasok",  getStatBuszFelszallasok(utazasAdat));

        bemenetek.put("Statisztika2: Jaratfelszallasok", getStatJaratFelszallasok(utazasAdat));

        bemenetek.put("Statisztika3: Megallofelszallasok", getStatMegalloFelszallasok(utazasAdat));

    }

    /**
     *
     * Egy kisstatisztika amelyben összesítjük az egyes megállók utasforgalmát (felszállások száma)
     *
     * @param utazasAdat az a rendszerezettt adatokat tároló UtazásiAdatok objektum aliasa
     *
     * @return visszaadjuk a kisstatisztikát tartalmazó JSONArray-t a feldolgoz methodusnak más kistatisztikákkal való összefűzésre
     */
    private JSONArray getStatMegalloFelszallasok(UtazasAdatok utazasAdat) {
        JSONArray megallok = new JSONArray();
        for (Megallo megallo : utazasAdat.getMegalloim().values()) {
            int felszallasSzam = megallo.getFelszallasok().size();
            megallok.add(String.format(" Megallo [%s] felszallasainak szama : [%s]", megallo.getMegalloId(), felszallasSzam));
        }
        return megallok;
    }

    /**
     *
     * Egy kisstatisztika amelyben összesítjük az egyes járatok utasforgalmát (felszállások száma)
     *
     * @param utazasAdat az a rendszerezettt adatokat tároló UtazásiAdatok objektum aliasa
     *
     * @return visszaadjuk a kisstatisztikát tartalmazó JSONArray-t a feldolgoz methodusnak más kistatisztikákkal való összefűzésre
     */
    private JSONArray getStatJaratFelszallasok(UtazasAdatok utazasAdat) {
        JSONArray jaratok = new JSONArray();
        for (Jarat jarat : utazasAdat.getJarataim().values()) {
            int felszallasSzam = jarat.getFelszallasok().size();
            jaratok.add(String.format(" Jarat [%s] felszallasainak szama : [%s]", jarat.getJaratId(), felszallasSzam));
        }
        return jaratok;
    }

    /**
     *
     * Egy kisstatisztika amelyben összesítjük az egyes Buszok utasforgalmát (felszállások száma).  |Edit: Lehet hogy nem a leghasznosabb statisztika épp ez így olvasva|
     *
     * @param utazasAdat az a rendszerezettt adatokat tároló UtazásiAdatok objektum aliasa
     *
     * @return visszaadjuk a kisstatisztikát tartalmazó JSONArray-t a feldolgoz methodusnak más kistatisztikákkal való összefűzésre
     */
    private JSONArray getStatBuszFelszallasok(UtazasAdatok utazasAdat) {
        JSONArray buszok = new JSONArray();
        for (Busz busz : utazasAdat.getBuszaim().values()) {
            int felszallasSzam = busz.getFelszallasok().size();
            buszok.add(String.format(" Busz [%s] felszallasainak szama : [%s]", busz.getRendszam(), felszallasSzam));
        }
        return buszok;
    }

    @Override
    public void outputKiirasa(String outFileNeve) throws IOException {
        try(FileWriter file = new FileWriter(outFileNeve)) {
            file.write(bemenetek.toJSONString());
        }
    }
}
